//
//  FirstViewController.m
//  AotteriTunes
//
//  Created by user on 2019/6/5.
//  Copyright © 2019 Alex. All rights reserved.
//

#import "FirstViewController.h"
#import "SearchTableViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/SDWebImage.h>

@interface FirstViewController () <UISearchBarDelegate>
{
NSMutableDictionary *dict;
NSMutableArray *currentRow;
NSMutableArray *movieRow;
NSMutableArray *musicRow;
    NSString *searchKey;
    long moiveResultCount,musicResultCount;
}
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [_searchTableView setHidden:YES];
}

- (void)refreshTable {
    //TODO: refresh your data
    [self->_searchTableView.refreshControl endRefreshing];
    [self.searchTableView reloadData];
}


- (IBAction)tapSearchBtn:(id)sender
{
    NSLog(@"2: %@",_txtSearch.text);
    if (![_txtSearch.text isEqual: @""])
    {
    searchKey = [_txtSearch.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    searchKey = [searchKey stringByReplacingOccurrencesOfString:@" "
                                   withString:@"+"];
    [self praseSearch];
    [self.searchTableView setNeedsLayout];
    [self.searchTableView layoutIfNeeded];
    [self->_searchTableView.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
//    [_searchTableView setHidden:NO];
    }
}

-(void)praseSearch
{
    //1.創建會話管理者
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    //https://itunes.apple.com/search?term=jack+johnson&entity=movie
    //
    NSMutableArray *paramDict = [@{
                                   @"term":searchKey
                                   ,@"entity":@"movie"
//                                   ,@"attribute":@"movieTerm"
                                   } mutableCopy];

    NSMutableArray *paramDict2 = [@{
                                   @"term":searchKey
                                   } mutableCopy];
//movie
    [manager GET:@"https://itunes.apple.com/search" parameters:paramDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
        NSLog(@"%@---%@",[responseObject class],responseObject);
            self->movieRow = responseObject[@"results"];
            [self refreshTable];
            self->moiveResultCount =  [responseObject[@"resultCount"] longLongValue];
            if (self->moiveResultCount <= 0)
            {
                NSLog(@"*movie not found!");
                [self->_searchTableView setHidden:YES];
            }
            else
            {
                [self->_searchTableView setHidden:NO];
            }
         }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"movie请求失败--%@",error);
    }];
    
//music
    [manager GET:@"https://itunes.apple.com/search" parameters:paramDict2 progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSLog(@"%@---%@",[responseObject class],responseObject);
            self->musicRow = responseObject[@"results"];
            [self refreshTable];
            self->musicResultCount =  [responseObject[@"resultCount"] longLongValue];
            if (self->musicResultCount <= 0)
            {
                NSLog(@"*movie not found!");
                [self->_searchTableView setHidden:YES];
            }
            else
            {
                [self->_searchTableView setHidden:NO];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"music请求失败--%@",error);
    }];

}


#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return movieRow.count;
    else if (section == 1)
        return musicRow.count;
    else
        return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return @"Moive";
    else if (section == 1)
        return @"Music";
    else
        return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchTableViewCell *cell = [SearchTableViewCell new];
    if (indexPath.section == 0)
   {
            if (self->moiveResultCount > 0)
            {
               cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
               NSString *url = movieRow[indexPath.row][@"artworkUrl60"];
               NSLog(@"url: %@",url);
               [cell.imgPicture sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"Pichoder"]];
                cell.lblTrackName.text = movieRow[indexPath.row][@"trackName"];
                cell.lblAristName.text = movieRow[indexPath.row][@"artistName"];
                cell.lblCollectionName.text = movieRow[indexPath.row][@"collectionCensoredName"];
                long h,m,s,tt;
                NSString *fmtString;
                tt = [movieRow[indexPath.row][@"trackTimeMillis"] longValue] /1000;
                h = tt /3600;
                m = tt %3600 /60;
                s = tt % 60;
                fmtString = [NSString stringWithFormat:@"%ldh: %ldm :%lds",h,m,s];
               cell.lblLength.text = fmtString;
               cell.lblLongDescription.text = movieRow[indexPath.row][@"longDescription"];
               [cell.btnReadMore setHidden:NO];
            }
            else{
                    NSLog(@"-moiveResultCount <= 0");
            }
     }else{
          if (self->musicResultCount > 0)
         {
             cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
             NSString *url = musicRow[indexPath.row][@"artworkUrl60"];
             NSLog(@"url: %@",url);
             [cell.imgPicture sd_setImageWithURL:[NSURL URLWithString:url]
                                placeholderImage:[UIImage imageNamed:@"Pichoder"]];
             cell.lblTrackName.text = musicRow[indexPath.row][@"trackName"];
             cell.lblAristName.text = musicRow[indexPath.row][@"artistName"];
             cell.lblCollectionName.text = musicRow[indexPath.row][@"collectionCensoredName"];
             long h,m,s,tt;
             NSString *fmtString;
             tt = [musicRow[indexPath.row][@"trackTimeMillis"] longValue] /1000;
             h = tt /3600;
             m = tt %3600 /60;
             s = tt % 60;
             fmtString = [NSString stringWithFormat:@"%ldh: %ldm :%lds",h,m,s];
             cell.lblLength.text = fmtString;
//             cell.lblLongDescription.text = musicRow[indexPath.row][@"longDescription"];
             [cell.lblLongDescription setHidden:YES];
             [cell.btnReadMore setHidden:YES];
         }
         else{
             NSLog(@"-musicResultCount <= 0");
         }
     }
    
//    }else{
//       cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
//       NSString *url = musicRow[indexPath.row][@"artworkUrl60"];
//       NSLog(@"url: %@",url);
//       [cell.imgPicture sd_setImageWithURL:[NSURL URLWithString:url]
//                          placeholderImage:[UIImage imageNamed:@"pichoder"]];
//
//       NSLog(@"%@", [[musicRow objectAtIndex:indexPath.row] valueForKey:[NSString stringWithFormat:@"trackName"]]);
//       cell.lblTrackName.text = musicRow[indexPath.row][@"trackName"];
//       cell.lblAristName.text = musicRow[indexPath.row][@"artistName"];
//       cell.lblCollectionName.text = musicRow[indexPath.row][@"collectionName"];
//       //--h:m:s
//       long h,m,s,tt;
//       NSString *fmtString;
//       tt = [musicRow[indexPath.row][@"trackTimeMillis"] longValue] /1000;
//       NSLog(@"tt: %ld",tt);
//       h = tt /3600;
//       m = tt %3600 /60;
//       s = tt % 60;
//       fmtString = [NSString stringWithFormat:@"%ldh: %ldm :%lds",h,m,s];
//       cell.lblLength.text = fmtString;
//       //--
//       cell.lblLongDescription.text = movieRow[indexPath.row][@"longDescription"];
//       [cell.btnReadMore setHidden:YES];
//   }
      return cell;
}

@end
