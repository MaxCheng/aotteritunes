//
//  FavroteViewController
//  AotteriTunes
//
//  Created by user on 2019/6/5.
//  Copyright © 2019 Alex. All rights reserved.
//

#import "FavroteViewController.h"
#import "SearchTableViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/SDWebImage.h>
#import "Settings.h"

@interface FavroteViewController ()
{
NSMutableDictionary *dict;
NSMutableArray *currentRow;
NSMutableArray *movieRow;
NSMutableArray *musicRow;
    NSString *searchKey;
    long moiveResultCount,musicResultCount;
}
@end

@implementation FavroteViewController

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"@viewDidApper");
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    // Initialize the refresh control.
    
    
     [self.view setBackgroundColor:[Settings sharedSettings].themeColor];
    
    _searchTableView.refreshControl = [[UIRefreshControl alloc] init];
    _searchTableView.refreshControl.backgroundColor = [UIColor purpleColor];
    _searchTableView.refreshControl.tintColor = [UIColor whiteColor];
    [_searchTableView.refreshControl addTarget:self action:@selector(reloadData2) forControlEvents:UIControlEventValueChanged];
    
    
    
    [_searchTableView setHidden:YES];
    _searchTableView.rowHeight = UITableViewAutomaticDimension;
//    _searchTableView.rowHeight = 190;
    _searchTableView.estimatedRowHeight = 190;//the estimatedRowHeight but if is more this autoincremented with autolayout

    [_searchTableView setNeedsLayout];
    [_searchTableView layoutIfNeeded];
}
-(void)reloadData2{
    [self refreshTable];
}

- (void)refreshTable {
    //TODO: refresh your data
    [self->_searchTableView.refreshControl endRefreshing];
    [self.searchTableView reloadData];
}



//-(void)praseSearch
//{
//    //1.創建會話管理者
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//
//    //https://itunes.apple.com/search?term=jack+johnson&entity=movie
//    //
//    NSMutableArray *paramDict = [@{
//                                   @"term":searchKey
//                                   ,@"entity":@"movie"
////                                   ,@"attribute":@"movieTerm"
//                                   } mutableCopy];
//
//
//    //https://itunes.apple.com/search?term=jack+johnson
//    //
//    NSMutableArray *paramDict2 = [@{
//                                   @"term":searchKey
//                                   } mutableCopy];
////movie
//
//    [manager GET:@"https://itunes.apple.com/search" parameters:paramDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//
//        if ([responseObject isKindOfClass:[NSDictionary class]])
//        {
//        NSLog(@"%@---%@",[responseObject class],responseObject);
//            self->movieRow = responseObject[@"results"];
//            [self refreshTable];
//            self->moiveResultCount =  [responseObject[@"resultCount"] longLongValue];
//            if (self->moiveResultCount <= 0)
//            {
//                NSLog(@"*movie not found!");
//                [self->_searchTableView setHidden:YES];
//            }
//            else
//            {
//                [self->_searchTableView setHidden:NO];
//            }
//         }
//
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"movie请求失败--%@",error);
//    }];
//
////music
//    [manager GET:@"https://itunes.apple.com/search" parameters:paramDict2 progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//
//        if ([responseObject isKindOfClass:[NSDictionary class]])
//        {
//            NSLog(@"%@---%@",[responseObject class],responseObject);
//            self->musicRow = responseObject[@"results"];
//            [self refreshTable];
//            self->musicResultCount =  [responseObject[@"resultCount"] longLongValue];
//            if (self->musicResultCount <= 0)
//            {
//                NSLog(@"*movie not found!");
//                [self->_searchTableView setHidden:YES];
//            }
//            else
//            {
//                [self->_searchTableView setHidden:NO];
//            }
//        }
//
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"music请求失败--%@",error);
//    }];
//
//}


#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0)
        return movieRow.count;
    else if (section == 1)
        return musicRow.count;
    else
        return 0;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return @"Moive";
    else if (section == 1)
        return @"Music";
    else
        return @"";
}

#pragma mark - Table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did select: %ld",(long)indexPath.row);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchTableViewCell *cell = [SearchTableViewCell new];
    if (indexPath.section == 0)
   {
            if (self->moiveResultCount > 0)
            {
               cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
               NSString *url = movieRow[indexPath.row][@"artworkUrl60"];
//               NSLog(@"url: %@",url);
               [cell.imgPicture sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"Pichoder"]];
                cell.lblTrackName.text = movieRow[indexPath.row][@"trackName"];
                cell.lblAristName.text = movieRow[indexPath.row][@"artistName"];
                cell.lblCollectionName.text = movieRow[indexPath.row][@"collectionCensoredName"];
                long h,m,s,tt;
                NSString *fmtString;
                tt = [movieRow[indexPath.row][@"trackTimeMillis"] longValue] /1000;
                h = tt /3600;
                m = tt %3600 /60;
                s = tt % 60;
                fmtString = [NSString stringWithFormat:@"%ldh: %ldm :%lds",h,m,s];
               cell.lblLength.text = fmtString;
               cell.lblLongDescription.text = movieRow[indexPath.row][@"longDescription"];
               cell.parterTableView =  _searchTableView;
                cell.trackId = [movieRow[indexPath.row][@"trackId"] longValue];
                cell.trackViewUrl = movieRow[indexPath.row][@"trackViewUrl"];
                [[Settings sharedSettings] setUrlAddress:cell.trackViewUrl];
                
              
                //--Favorite
                  long lTrackId = [movieRow[indexPath.row][@"trackId"] longValue];
                NSString *favIdKey = [[NSNumber numberWithLong:lTrackId] stringValue];
                NSLog(@"*favIdKey: %@",favIdKey);
        
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *favIdSaved= [defaults objectForKey: favIdKey ];
                
                NSLog(@"count: %li",(long)defaults.accessibilityElementCount);
                
                NSLog(@"%@",favIdSaved);
                
                if (favIdSaved == NULL)     //目前為未收藏
                {
                    [cell.btnFavorte setTitle:@"收藏" forState:UIControlStateNormal];
                    NSLog(@"*-Not Have favIDed");
                }else{                      //已經有收藏
                    [cell.btnFavorte setTitle:@"取消收藏" forState:UIControlStateNormal];
                    NSLog(@"*-Have favIDed");
                }
                //--
                [cell.btnReadMore setHidden:NO];
            }
            else{
                    NSLog(@"-moiveResultCount <= 0");
            }
         [cell reloadInputViews];
     }else{
          if (self->musicResultCount > 0)
         {
             cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
             NSString *url = musicRow[indexPath.row][@"artworkUrl60"];
             NSLog(@"url: %@",url);
             [cell.imgPicture sd_setImageWithURL:[NSURL URLWithString:url]
                                placeholderImage:[UIImage imageNamed:@"Pichoder"]];
             cell.lblTrackName.text = musicRow[indexPath.row][@"trackName"];
             cell.lblAristName.text = musicRow[indexPath.row][@"artistName"];
             cell.lblCollectionName.text = musicRow[indexPath.row][@"collectionCensoredName"];
             long h,m,s,tt;
             NSString *fmtString;
             tt = [musicRow[indexPath.row][@"trackTimeMillis"] longValue] /1000;
             h = tt /3600;
             m = tt %3600 /60;
             s = tt % 60;
             fmtString = [NSString stringWithFormat:@"%ldh: %ldm :%lds",h,m,s];
             cell.lblLength.text = fmtString;
 
             //--Favorite
             long lTrackId = [musicRow[indexPath.row][@"trackId"] longValue];
             NSString *favIdKey = [[NSNumber numberWithLong:lTrackId] stringValue];
             NSLog(@"*favIdKey: %@",favIdKey);
             
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             NSString *favIdSaved= [defaults objectForKey: favIdKey ];
             
             NSLog(@"music count: %li",(long)defaults.accessibilityElementCount);
             
             NSLog(@"%@",favIdSaved);
             
             if (favIdSaved == NULL)     //目前為未收藏
             {
                 [cell.btnFavorte setTitle:@"收藏" forState:UIControlStateNormal];
                 NSLog(@"*-Not Have favIDed");
             }else{                      //已經有收藏
                 [cell.btnFavorte setTitle:@"取消收藏" forState:UIControlStateNormal];
                 NSLog(@"*-Have favIDed");
             }
             //--
             
             
             [cell.lblLongDescription setHidden:YES];
             [cell.btnReadMore setHidden:YES];
         }
         else{
             NSLog(@"-musicResultCount <= 0");
         }
       
     }
    
//    }else{
//       cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
//       NSString *url = musicRow[indexPath.row][@"artworkUrl60"];
//       NSLog(@"url: %@",url);
//       [cell.imgPicture sd_setImageWithURL:[NSURL URLWithString:url]
//                          placeholderImage:[UIImage imageNamed:@"pichoder"]];
//
//       NSLog(@"%@", [[musicRow objectAtIndex:indexPath.row] valueForKey:[NSString stringWithFormat:@"trackName"]]);
//       cell.lblTrackName.text = musicRow[indexPath.row][@"trackName"];
//       cell.lblAristName.text = musicRow[indexPath.row][@"artistName"];
//       cell.lblCollectionName.text = musicRow[indexPath.row][@"collectionName"];
//       //--h:m:s
//       long h,m,s,tt;
//       NSString *fmtString;
//       tt = [musicRow[indexPath.row][@"trackTimeMillis"] longValue] /1000;
//       NSLog(@"tt: %ld",tt);
//       h = tt /3600;
//       m = tt %3600 /60;
//       s = tt % 60;
//       fmtString = [NSString stringWithFormat:@"%ldh: %ldm :%lds",h,m,s];
//       cell.lblLength.text = fmtString;
//       //--
//       cell.lblLongDescription.text = movieRow[indexPath.row][@"longDescription"];
//       [cell.btnReadMore setHidden:YES];
//   }
      return cell;
}

@end
