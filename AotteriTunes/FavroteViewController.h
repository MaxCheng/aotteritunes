//
//  FirstViewController.h
//  AotteriTunes
//
//  Created by user on 2019/6/5.
//  Copyright © 2019 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavroteViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *searchTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *itemSegmentControl;

@end

