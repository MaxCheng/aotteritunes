//
//  WKWebViewController.h
//  AotteriTunes
//
//  Created by user on 2019/6/5.
//  Copyright © 2019 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WKWebViewController : UIViewController
@property (nonatomic, retain) IBOutlet WKWebView *webView;
@property (strong, nonatomic) NSString *productURL;
@end

NS_ASSUME_NONNULL_END
